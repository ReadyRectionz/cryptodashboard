## Screenshots of the application
![Funds overview](./screenshots/Funds.png)
![Graphs](./screenshots/Archimedes_Graphs1_20240409153000.png)
![Table](./screenshots/Archimedes_Prot1_20240409153000.png)
![Calanders](./screenshots/Archimedes_Wallet_20240409153000.png)



See the folder `screenshots` for more.

## Getting Started

First, run the development server:

```bash
npm i

npm run dev

```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Connection with Supabase and Debank

For a full user experience a Supabase and Debank Api connection is required. The table overview can be found in `src/lib/database/types.ts`.

For more information please contact us at [info@levelstech.nl](mailto:info@levelstech.nl) or visit [info@levelstech.nl](http://www.levelstech.nl/)