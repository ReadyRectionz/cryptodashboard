export interface Chain {
  id: string;
  community_id: number;
  name: string;
  native_token_id: string;
  logo_url: string;
  wrapped_token_id: string;
  is_support_pre_exec?: boolean;
  usd_value: number;
}

export interface TotalBalanceResponse {
  total_usd_value: number;
  chain_list: Chain[];
}

interface Pool {
  id: string;
  chain: string;
  project_id: string;
  adapter_id: string;
  controller: string;
  index: string | null;
  time_at: number;
}

export interface Token {
  id: string;
  chain: string;
  name: string;
  symbol: string;
  display_symbol: string | null;
  optimized_symbol: string;
  decimals: number;
  logo_url: string | null;
  protocol_id: string;
  price: number;
  is_verified: boolean;
  is_core: boolean;
  is_wallet: boolean;
  time_at: number | null;
  amount: number;
}
//https://docs.cloud.debank.com/en/readme/api-models/portfolioitemobject
interface PortfolioItem {
  stats: {
    asset_usd_value: number;
    debt_usd_value: number;
    net_usd_value: number;
  };
  asset_dict: {
    [key: string]: number;
  };
  asset_token_list: Token[];
  update_at: number;
  name: string;
  detail_types: string[];
  detail: {
    supply_token_list?: Token[];
    reward_token_list?: Token[];
    borrow_token_list?: Token[];
  };
  pool?: Pool;
}
// https://docs.cloud.debank.com/en/readme/api-pro-reference/user#get-user-complex-protocol-list
export interface ComplexProtocol {
  id: string;
  chain: string;
  name: string | null;
  logo_url: string;
  site_url: string;
  has_supported_portfolio: boolean;
  tvl: number;
  portfolio_item_list: PortfolioItem[];
}

export interface NormalToken {
  chain: string;
  credit_score: number;
  decimals: number;
  display_symbol: string | null;
  id: string;
  is_core: boolean | null;
  is_scam: boolean;
  is_suspicious: boolean;
  is_verified: boolean | null;
  is_wallet: boolean;
  logo_url: string | null;
  name: string;
  optimized_symbol: string;
  price: number;
  price_24h_change: number | null;
  protocol_id: string;
  symbol: string;
  time_at: number | null;
}
export interface DifferentToken {
  attributes: {
    key: string;
    trait_type: string;
    value: string;
  }[];
  chain: string;
  collection: {
    chain: string;
    credit_score: number;
    description: string | null;
    floor_price: number;
    id: string;
    is_core: boolean;
    is_scam: boolean;
    is_suspicious: boolean;
    is_verified: boolean;
    logo_url: string;
    name: string;
  };
  collection_id: string;
  content: string;
  content_type: string | null;
  contract_id: string;
  description: string | null;
  detail_url: string;
  id: string;
  inner_id: string;
  is_erc1155: boolean;
  is_erc721: boolean;
  name: string;
  pay_token: string | null;
  symbol: string;
  thumbnail_url: string;
  total_supply: number;
}

export type TokenFromTokenDict = NormalToken | DifferentToken;

export interface WalletHistroyDicts {
  cate_dict: {
    [key: string]: { id: string; name: string };
  };
  cex_dict: {
    [key: string]: Object;
  };
  project_dict: {
    [key: string]: {
      chain: string;
      id: string;
      logo_url: string;
      name: string;
      site_url: string;
    };
  };
  token_dict: {
    [key: string]: TokenFromTokenDict;
  };
}

export interface WalletHistory {
  cate_dict: {
    [key: string]: { id: string; name: string };
  };
  cex_dict: {
    [key: string]: Object;
  };
  project_dict: {
    [key: string]: {
      chain: string;
      id: string;
      logo_url: string;
      name: string;
      site_url: string;
    };
  };
  token_dict: {
    [key: string]: TokenFromTokenDict;
  };
  history_list: HistoryListItem[];
}

export interface HistoryListItem {
  cate_id: string | null;
  cex_id: string | null;
  chain: string;
  id: string;
  is_scam: boolean;
  other_addr: string | null;
  project_id: string | null;
  receives?: {
    amount: number;
    from_addr: string;
    token_id: string;
  }[];
  sends?: {
    amount: number;
    to_addr: string;
    token_id: string;
  }[];
  time_at: number;
  token_approve: {
    spender: string;
    token_id: string;
    value: number;
  } | null;
  tx: HistoryItemTx | null;
}

export interface HistoryItemTx {
  eth_gas_fee?: number;
  from_addr: string;
  message?: string | null;
  name: string;
  params: string[];
  selector: string;
  status: number;
  to_addr: string;
  usd_gas_fee?: number;
  value: number;
}
