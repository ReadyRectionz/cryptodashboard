'use server';

import {
  PROTOCOL_DATA,
  TRANSATION_DATA,
  TOTAL_BALANCE_DATA,
  TOKEN_BALANCE_DATA,
  NETCURVE_DATA,
} from '@/utils/cache/data';
import {
  ComplexProtocol,
  WalletHistory,
  Token,
  TotalBalanceResponse,
  HistoryListItem,
} from './type';

const getRandomXitemsFromArray = <T>(x: number, arr: T[]): T[] => {
  const randomSize = Math.floor(Math.random() * x);

  const result = [];

  for (let i = 0; i < randomSize; i++) {
    const randomIndex = Math.floor(Math.random() * arr.length);
    result.push(arr[randomIndex]);
  }
  return result;
};
const getXRandomitemsFromArray = <T>(x: number, arr: T[]): T[] => {
  const result = [];

  for (let i = 0; i < x; i++) {
    const randomIndex = Math.floor(Math.random() * arr.length);
    result.push(arr[randomIndex]);
  }
  return result;
};

export const debankApiCall = async (path: string, params: object) => {
  const paramString = Array.from(Object.entries(params)).reduce(
    (prevVal, currVal, idx) => {
      const sValue = currVal[0] + '=' + currVal[1];
      return idx == 0 ? sValue : prevVal + '&' + sValue;
    },
    '',
  );
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_DEBANK_API_URL}${path}?${paramString}`,
    {
      method: 'GET',
      headers: {
        AccessKey: `${process.env.NEXT_PUBLIC_DEBANK_API_KEY}`,
        Accept: 'application/json',
      },
    },
  );
  console.debug('[DEBUG] debank call', path);
  return await response.json();
};

export const fetchWalletChainBalances = async (
  pubkey: string,
): Promise<TotalBalanceResponse> => {
  if (!process.env.USE_DEBANK_REALTIME) {
    console.debug('[DEBUG] spoofed api call user/total_balance');
    const chainList = getRandomXitemsFromArray(
      3,
      TOTAL_BALANCE_DATA.chain_list,
    );

    const totalUsdValue = chainList.reduce((acc, x) => acc + x.usd_value, 0);
    return {
      total_usd_value: totalUsdValue,
      chain_list: chainList,
    };
  }
  return await debankApiCall('user/total_balance', { id: pubkey });
};

export const fetchWalletTotalBalance = async (pubkey: string) => {
  const response = await fetchWalletChainBalances(pubkey);
  return response.total_usd_value;
};

export const fetchWalletProtocols = async (
  pubkey: string,
): Promise<ComplexProtocol[]> => {
  if (!process.env.USE_DEBANK_REALTIME) {
    console.debug('[DEBUG] spoofed api call user/all_complex_protocol_list');
    return getRandomXitemsFromArray(3, PROTOCOL_DATA);
  }
  const response = await debankApiCall('user/all_complex_protocol_list', {
    id: pubkey,
  });

  return await response;
};

export const fetchWalletsTokens = async (pubkey: string): Promise<Token[]> => {
  if (!process.env.USE_DEBANK_REALTIME) {
    console.debug('[DEBUG] spoofed api call user/all_token_list');
    return getRandomXitemsFromArray(20, TOKEN_BALANCE_DATA);
  }

  return await debankApiCall('user/all_token_list', {
    id: pubkey,
    //is_all: false,
  });
};

export const fetchWalletHistory = async (
  pubkey: string,
  page_count: number,
  start_time: number,
): Promise<WalletHistory> => {
  if (!process.env.USE_DEBANK_REALTIME) {
    console.debug('[DEBUG] spoofed api call user/all_history_list');
    let res = TRANSATION_DATA;

    res.history_list = getXRandomitemsFromArray(
      page_count,
      TRANSATION_DATA.history_list,
    );
    return res;
  }

  const response = await debankApiCall('user/all_history_list', {
    id: pubkey,
    page_count,
    start_time,
  });

  return await response;
};

const parseSumarizedDate = (
  acc: { [key: string]: number },
  x: HistoryListItem,
) => {
  const time = new Date(x.time_at * 1000);
  const dayKey = time.toISOString().slice(0, 10);
  acc[dayKey] = acc[dayKey] || 0;
  acc[dayKey] += 1;
  return acc;
};

export const fetchWalletHistorySumarized = async (
  pubkey: string,
): Promise<any> => {
  if (!process.env.USE_DEBANK_REALTIME) {
    console.debug('[DEBUG] spoofed api call user/all_history_list');
    const hList = getXRandomitemsFromArray(20, TRANSATION_DATA.history_list);

    return hList.reduce(parseSumarizedDate, {});
  }

  const response = await debankApiCall('user/all_history_list', {
    id: pubkey,
  });

  return await response.history_list.reduce(parseSumarizedDate, {});
};

export const fetchNetCurve = async (
  pubkey: string,
): Promise<{ timestamp: number; usd_value: number }[]> => {
  if (!process.env.USE_DEBANK_REALTIME) {
    console.debug('[DEBUG] spoofed api call user/total_net_curve');
    return NETCURVE_DATA.slice(0, 10).map((x) => ({
      ...x,
      usd_value: Math.random() * x.usd_value,
    }));
  }

  return await debankApiCall('user/total_net_curve', {
    id: pubkey,
  });
};
