import { type ClassValue, clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const valueFormat = (value?: number): string => {
  return `$${Number(value).toLocaleString('nl-NL', {
    maximumFractionDigits: 2,
    minimumFractionDigits: 2,
  })}`;
};
