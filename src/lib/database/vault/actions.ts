'use server';
import supabaseServerSide from '@/utils/supabase/server';

export const readSecret = async (secret_uuid: string) => {
  const { supabase, error } = await supabaseServerSide();

  if (!supabase) {
    return {
      data: null,
      error,
    };
  }

  return await supabase.rpc('read_secret', {
    secret_uuid,
  });
};

export const insertSecret = async (name: string, secret: string) => {
  const { supabase, error } = await supabaseServerSide();

  if (!supabase) {
    return {
      data: null,
      error,
    };
  }

  return await supabase.rpc('insert_secret', {
    name,
    secret,
  });
};
