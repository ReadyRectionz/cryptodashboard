'use server';
import supabaseServerSide from '@/utils/supabase/server';

export const fetchFundByUUID = async (uuid: string) => {
  const { supabase, error } = await supabaseServerSide();
  if (!supabase) {
    return {
      data: null,
      error,
    };
  }
  return await supabase.from('fund').select('*').eq('uuid', uuid);
};

export const fetchFundIdWithWalletsByUUID = async (uuid: string) => {
  const { supabase, error } = await supabaseServerSide();
  if (!supabase) {
    return {
      data: null,
      error,
    };
  }
  return await supabase
    .from('fund')
    .select('*, wallet(id, name, pubkey_id, active)')
    .eq('uuid', uuid);
};

export const fetchFundsWithWalletsPubkey = async () => {
  const { supabase, error } = await supabaseServerSide();
  if (!supabase) {
    return {
      data: null,
      error,
    };
  }
  return await supabase
    .from('fund')
    .select('*,  wallet(id, name, pubkey_id, active)');
};
