import { WalletDBFull } from '../wallet/type';

export interface FundDB {
  created_at: string;
  description: string | null;
  id: number;
  min_amount: number;
  name: string;
  source_of_funds_min_amount: number;
  uuid: string;
}

export interface FundIdWithFullWallet {
  id: number;
  name?: string;
  uuid?: string;
  description?: string;
  wallet: WalletDBFull[];
}
