export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[];

export interface Database {
  public: {
    Tables: {
      fund: {
        Row: {
          created_at: string;
          description: string | null;
          id: number;
          min_amount: number;
          name: string;
          source_of_funds_min_amount: number;
          uuid: string;
        };
        Insert: {
          created_at?: string;
          description?: string | null;
          id?: number;
          name: string;
          uuid?: string;
        };
        Update: {
          created_at?: string;
          description?: string | null;
          id?: number;
          name?: string;
          uuid?: string;
        };
        Relationships: [];
      };
      wallet: {
        Row: {
          active: boolean;
          created_at: string;
          id: number;
          name: string;
          pubkey_id: string;
        };
        Insert: {
          active?: boolean;
          created_at?: string;
          id?: number;
          name: string;
          pubkey_id: string;
        };
        Update: {
          active?: boolean;
          created_at?: string;
          id?: number;
          name?: string;
          pubkey_id?: string;
        };
        Relationships: [
          {
            foreignKeyName: 'wallet_pubkey_id_fkey';
            columns: ['pubkey_id'];
            isOneToOne: false;
            referencedRelation: 'decrypted_secrets';
            referencedColumns: ['id'];
          },
          {
            foreignKeyName: 'wallet_pubkey_id_fkey';
            columns: ['pubkey_id'];
            isOneToOne: false;
            referencedRelation: 'secrets';
            referencedColumns: ['id'];
          },
        ];
      };
      wallet_in_fund: {
        Row: {
          fund_id: number;
          wallet_id: number;
        };
        Insert: {
          fund_id: number;
          wallet_id: number;
        };
        Update: {
          fund_id?: number;
          wallet_id?: number;
        };
        Relationships: [
          {
            foreignKeyName: 'wallet_in_fund_fund_id_fkey';
            columns: ['fund_id'];
            isOneToOne: false;
            referencedRelation: 'fund';
            referencedColumns: ['id'];
          },
          {
            foreignKeyName: 'wallet_in_fund_wallet_id_fkey';
            columns: ['wallet_id'];
            isOneToOne: false;
            referencedRelation: 'wallet';
            referencedColumns: ['id'];
          },
        ];
      };
      wallet_snapshot: {
        Row: {
          created_at: string;
          daily_snapshot: boolean;
          id: number;
          wallet_id: number;
        };
        Insert: {
          created_at?: string;
          daily_snapshot?: boolean;
          id?: number;
          wallet_id: number;
        };
        Update: {
          created_at?: string;
          daily_snapshot?: boolean;
          id?: number;
          wallet_id?: number;
        };
        Relationships: [
          {
            foreignKeyName: 'wallet_snapshot_wallet_id_fkey';
            columns: ['wallet_id'];
            isOneToOne: false;
            referencedRelation: 'wallet';
            referencedColumns: ['id'];
          },
        ];
      };
      wallet_value_snapshot: {
        Row: {
          chain_id: number;
          created_at: string;
          currency: string;
          id: number;
          value: number;
          wallet_snapshot_id: number;
        };
        Insert: {
          chain_id: number;
          created_at?: string;
          currency: string;
          id?: number;
          value: number;
          wallet_snapshot_id: number;
        };
        Update: {
          chain_id?: number;
          created_at?: string;
          currency?: string;
          id?: number;
          value?: number;
          wallet_snapshot_id?: number;
        };
        Relationships: [
          {
            foreignKeyName: 'wallet_value_snapshot_chain_id_fkey';
            columns: ['chain_id'];
            isOneToOne: false;
            referencedRelation: 'chain';
            referencedColumns: ['id'];
          },
          {
            foreignKeyName: 'wallet_value_snapshot_wallet_snapshot_id_fkey';
            columns: ['wallet_snapshot_id'];
            isOneToOne: false;
            referencedRelation: 'wallet_snapshot';
            referencedColumns: ['id'];
          },
        ];
      };
    };
    Views: {
      fund_share: {
        Row: {
          created_at: string | null;
          fund_id: number | null;
          number_shares: number | null;
          share_value: number | null;
          total_fund_value: number | null;
          total_shares: number | null;
        };
        Relationships: [
          {
            foreignKeyName: 'sepa_transaction_fund_id_fkey';
            columns: ['fund_id'];
            isOneToOne: false;
            referencedRelation: 'fund';
            referencedColumns: ['id'];
          },
        ];
      };
      fund_share_per_client: {
        Row: {
          client_fund_value: number | null;
          client_id: number | null;
          created_at: string | null;
          fund_id: number | null;
          number_shares: number | null;
          share_value: number | null;
          total_fund_value: number | null;
          total_shares: number | null;
        };
        Relationships: [
          {
            foreignKeyName: 'sepa_transaction_client_id_fkey';
            columns: ['client_id'];
            isOneToOne: false;
            referencedRelation: 'client';
            referencedColumns: ['id'];
          },
          {
            foreignKeyName: 'sepa_transaction_fund_id_fkey';
            columns: ['fund_id'];
            isOneToOne: false;
            referencedRelation: 'fund';
            referencedColumns: ['id'];
          },
        ];
      };
      share_data_cumulative: {
        Row: {
          fulldate: string | null;
          fund_id: number | null;
          number_shares: number | null;
        };
        Relationships: [
          {
            foreignKeyName: 'sepa_transaction_fund_id_fkey';
            columns: ['fund_id'];
            isOneToOne: false;
            referencedRelation: 'fund';
            referencedColumns: ['id'];
          },
        ];
      };
    };
    Functions: {
      read_secret: {
        Args: {
          secret_uuid: string;
        };
        Returns: string;
      };
      insert_secret: {
        Args: {
          name: string;
          secret: string;
        };
        Returns: string;
      };
    };
    Enums: {
      [_ in never]: never;
    };
    CompositeTypes: {
      [_ in never]: never;
    };
  };
}
