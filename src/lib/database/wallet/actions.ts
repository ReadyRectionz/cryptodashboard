import supabaseServerSide from '@/utils/supabase/server';
import { insertSecret, readSecret } from '../vault/actions';
import { WalletDBExtended, WalletDBExtendedWithPubkey } from './type';

export const decryptWalletPubkey = async (wallet: WalletDBExtended) => {
  if (!('pubkey' in wallet)) {
    const { data } = await readSecret(wallet.pubkey_id);

    if (data) wallet.pubkey = data;
  }

  return wallet;
};
export const fetchWalletPubkey = async (wallet: WalletDBExtended) => {
  if (!('pubkey' in wallet)) {
    const { data } = await readSecret(wallet.pubkey_id);

    return data;
  }

  return wallet.pubkey;
};

export const insertWallet = async (
  fund_id: number,
  name: string,
  pubkey: string,
) => {
  const { supabase, error: connectError } = await supabaseServerSide();
  if (!supabase) {
    return { data: null, error: connectError };
  }

  const { data: key, error: keyError } = await insertSecret(name, pubkey);
  if (keyError || !key) {
    return { data: null, error: keyError };
  }

  const { data: wallet, error: walletError } = await supabase
    .from('wallet')
    .insert([{ pubkey_id: key, name }])
    .select();

  if (walletError || !wallet) {
    return { data: null, error: walletError };
  }

  const { error } = await supabase
    .from('wallet_in_fund')
    .insert([{ fund_id, wallet_id: wallet[0].id }])
    .select();

  if (error) {
    return { data: null, error: error };
  }
};

export const getWalletSnapshots = async (wallet_id: number) => {
  const { supabase, error: connectError } = await supabaseServerSide();
  if (!supabase) {
    return {
      data: null,
      error: connectError,
    };
  }

  const { data, error } = await supabase
    .from('wallet_snapshot')
    .select('*, wallet_value_snapshot(*)')
    .eq('wallet_id', wallet_id)
    .order('created_at', { ascending: false });

  if (!data || error) {
    return { data: null, error };
  }

  return {
    data: data
      .map((x) => ({
        time: new Date(x.created_at).toDateString(),
        value: x.wallet_value_snapshot.reduce((acc, y) => (acc += y.value), 0),
      }))
      .slice(0, 31),
    error,
  };
};

export const setWalletActive = async (wallet_id: number, active: boolean) => {
  const { supabase, error } = await supabaseServerSide();
  if (!supabase) {
    return {
      data: null,
      error,
    };
  }
  return await supabase
    .from('wallet')
    .update({ active })
    .eq('id', wallet_id)
    .select();
};

export const deleteWallet = async (wallet_id: number) => {
  const { supabase, error } = await supabaseServerSide();
  if (!supabase) {
    return {
      data: null,
      error,
    };
  }
  return await supabase.from('wallet').delete().eq('id', wallet_id).select();
};
