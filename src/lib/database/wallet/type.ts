export interface WalletDB {
  id: number;
  name: string;
  pubkey_id: string;
  active: boolean;
}

export interface WalletDBExtended extends WalletDB {
  pubkey?: string;
  balance?: number;
}

export interface WalletDBExtendedWithPubkey extends WalletDB {
  pubkey: string;
}

export interface WalletDBFull extends WalletDB {
  pubkey: string;
  balance: number;
}
