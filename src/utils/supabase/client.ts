import { Database } from '@/lib/database/types';
import { createClientComponentClient } from '@supabase/auth-helpers-nextjs';

const supabaseClientSide = createClientComponentClient<Database>();
export default supabaseClientSide;
