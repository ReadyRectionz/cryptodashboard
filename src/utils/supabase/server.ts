'use server';
// This only works server-side!
import { Database } from '@/lib/database/types';
import { createServerActionClient } from '@supabase/auth-helpers-nextjs';
import { cookies } from 'next/headers';

async function supabaseServerSide() {
  try {
    const cookiesStore = cookies();

    const supabase = createServerActionClient<Database>({
      cookies: () => cookiesStore,
    });

    return {
      supabase,
      error: null,
    };
  } catch (e) {
    console.log('error in supabaseServerSide', e);

    return {
      supabase: null,
      error: e,
    };
  }
}

export default supabaseServerSide;
