import {
  DifferentToken,
  NormalToken,
  Token,
  TokenFromTokenDict,
} from '@/lib/debank/totalBalance/type';
import Image from 'next/image';

export const mapToken = (x: Token, i: number) => {
  return (
    <div key={`${i}`}>
      <div className="flex gap-2">
        {x?.logo_url && (
          <Image src={x?.logo_url} width={20} height={20} alt="logo" />
        )}
        <p className="line-clamp-1">
          {x?.name}
          {/* {x.token_id} */}
        </p>
      </div>
      <p>
        {x.amount.toFixed(2)} {x?.optimized_symbol}
      </p>
    </div>
  );
};

export const mapTokenFromDict = (
  token: TokenFromTokenDict,
  sender: string,
  value: number,
  idx: number,
) => {
  const logoUrl = (token as DifferentToken)?.collection
    ? (token as DifferentToken).collection.logo_url
    : (token as NormalToken)?.logo_url;
  const symbol = (token as DifferentToken)?.collection
    ? (token as DifferentToken).symbol
    : (token as NormalToken)?.optimized_symbol;

  return (
    <div key={idx}>
      <div className="flex gap-2">
        {logoUrl && <Image src={logoUrl} width={20} height={20} alt="logo" />}
        <p>
          {token?.name}
          {/* {x.token_id} */}
        </p>
      </div>
      <p className="text-xs truncate max-w-20">{sender}</p>
      <p>
        {value.toFixed(2)} {symbol}
      </p>
    </div>
  );
};
