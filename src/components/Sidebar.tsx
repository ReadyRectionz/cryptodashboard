'use client';

import Image from 'next/image';
import {
  Command,
  CommandGroup,
  CommandItem,
  CommandList,
  CommandSeparator,
} from './ui/command';
import { Bell, LineChart, LogOut, Settings, UserCog } from 'lucide-react';
import Link from 'next/link';

import supabase from '@/utils/supabase/client';
import { useRouter } from 'next/navigation';

const menuList = [
  {
    group: 'Dashboard',
    items: [
      {
        name: 'Funds',
        icon: <LineChart />,
        url: '/funds',
      },
      {
        name: 'Notifications',
        icon: <Bell />,
        url: '/notifications',
      },
    ],
  },
  {
    group: 'Settings',
    items: [
      {
        name: 'General Settings',
        icon: <Settings />,
        url: '/settings',
      },
      {
        name: 'Account Settings',
        icon: <UserCog />,
        url: '/account',
      },
    ],
  },
];
const Sidebar = () => {
  const router = useRouter();

  const signOut = async () => {
    await supabase.auth.signOut();
    router.refresh();
    router.push('/login');
  };

  return (
    <>
      <div className="flex flex-col fixed flex-1 gap-4 w-60 max-w-60 bg-[background] border-r-2  min-h-screen p-4">
        <Image
          src="/assets/logo/levelstech.png"
          width={250}
          height={400}
          alt="Company logo"
        />
        <Command style={{ overflow: 'visible' }}>
          <CommandList style={{ overflow: 'visible' }}>
            {menuList.map((menu: any, key: number) => {
              return (
                <div key={key}>
                  <CommandGroup heading={menu.group} />
                  {menu.items.map((item: any, iKey: number) => (
                    <CommandItem key={`ci-${key}${iKey}`}>
                      <Link
                        href={item.url}
                        className="flex gap-2 cursor-pointer"
                      >
                        {item.icon} {item.name}
                      </Link>
                    </CommandItem>
                  ))}
                </div>
              );
            })}
            <CommandSeparator />
            <CommandGroup>
              <CommandItem className="flex gap-2 cursor-pointer">
                <div className="flex flex-row" onClick={signOut}>
                  <LogOut /> Sign out
                </div>
              </CommandItem>
            </CommandGroup>
          </CommandList>
        </Command>
      </div>
      <div className="flex flex-col flex-1 gap-4 w-60 min-w-60 max-w-60 bg-[background] border-r-2  min-h-screen p-4"></div>
    </>
  );
};

export default Sidebar;
