'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { useRouter } from 'next/navigation';

import { Button } from '@/components/ui/button';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { Toaster } from '@/components/ui/toaster';
import { useToast } from '@/components/ui/use-toast';

import supabase from '@/utils/supabase/client';

// Move to supabase helper functions?
const handleSignIn = async (values: { email: string; password: string }) => {
  const { email, password } = values;

  return await supabase.auth.signInWithPassword({
    email,
    password,
  });
};

const formSchema = z.object({
  email: z.string().min(8, {
    message: 'Email must be at least 8 characters.',
  }),
  password: z.string().min(1, {
    message: 'Password cannot be empty.',
  }),
});

const LoginForm = () => {
  const router = useRouter();
  const { toast } = useToast();

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: '',
      password: '',
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    const { data: user, error } = await handleSignIn(values);

    if (!error) {
      router.push(`${location.origin}/funds`);
    } else {
      console.error(error);
      toast({
        title: 'Invalid credentials',
        description: 'Could not sign in',
      });
    }
  }
  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
        <FormField
          control={form.control}
          name="email"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Email</FormLabel>
              <FormControl>
                <Input placeholder="admin" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="password"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Password</FormLabel>
              <FormControl>
                <Input placeholder="*******" type="password" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button type="submit">Login</Button>
      </form>
      <Toaster />
    </Form>
  );
};

export default LoginForm;
