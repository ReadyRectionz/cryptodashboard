'use client';

import * as React from 'react';
import {
  ColumnFiltersState,
  PaginationState,
  SortingState,
  VisibilityState,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from '@tanstack/react-table';

import { keepPreviousData, useQuery } from '@tanstack/react-query';

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { Switch } from '@/components/ui/switch';
import { DataTableViewOptions } from './data-table-view-options';
import { DataTablePagination } from './data-table-pagination';
import { WalletHistroyDicts } from '@/lib/debank/totalBalance/type';
import Loading from '@/app/loading';

interface DataTableProps<TData, TValue> {
  fetchData: (pagination: PaginationState) => any;
  fetchColumns: (dicts: WalletHistroyDicts) => any;
}

export function DataTable<TData, TValue>({
  fetchData,
  fetchColumns,
}: DataTableProps<TData, TValue>) {
  const [sorting, setSorting] = React.useState<SortingState>([]);
  const [columnFilters, setColumnFilters] = React.useState<ColumnFiltersState>(
    [],
  );
  const [columnVisibility, setColumnVisibility] =
    React.useState<VisibilityState>({});

  const [pagination, setPagination] = React.useState<PaginationState>({
    pageIndex: 0,
    pageSize: 10,
  });
  const dataQuery = useQuery({
    queryKey: ['data', pagination],
    queryFn: () => fetchData(pagination),
    placeholderData: keepPreviousData, // don't have 0 rows flash while changing pages/loading next page
  });

  const defaultData = React.useMemo(() => [], []);

  const table = useReactTable({
    data: dataQuery.data?.rows ?? defaultData,
    columns: fetchColumns(dataQuery.data?.dicts ?? {}),
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),

    onSortingChange: setSorting,
    getSortedRowModel: getSortedRowModel(),

    onColumnFiltersChange: setColumnFilters,
    getFilteredRowModel: getFilteredRowModel(),

    onColumnVisibilityChange: setColumnVisibility,

    onPaginationChange: setPagination,

    manualPagination: true,
    autoResetPageIndex: false,

    state: {
      sorting,
      pagination,
      columnFilters,
      columnVisibility,
    },
  });

  const hasEntriesInTable = table.getRowModel().rows?.length;
  return (
    <div
      className={`${hasEntriesInTable ? 'bg-[#D9F3E2]' : 'bg-gray-50'} rounded-xl p-2`}
    >
      <div className="flex items-center py-4 px-6 gap-3">
        <div className="items-center flex-1">
          <Input
            placeholder="Filter chain..."
            disabled={!hasEntriesInTable}
            value={(table.getColumn('chain')?.getFilterValue() as string) ?? ''}
            onChange={(event) =>
              table.getColumn('chain')?.setFilterValue(event.target.value)
            }
            className="max-w-sm"
          />
        </div>
        <div className="flex items-center space-x-2 ">
          <Switch
            id="hide-scam"
            disabled={!hasEntriesInTable}
            onCheckedChange={(value) => {
              if (value) table.getColumn('is_scam')?.setFilterValue(!value);
              else table.getColumn('is_scam')?.setFilterValue(null);
            }}
          />
          <Label htmlFor="hide-scam">Hide scam</Label>
        </div>

        <div className="items-center">
          <DataTableViewOptions table={table} disabled={!hasEntriesInTable} />
        </div>
      </div>

      <div className="rounded-md border">
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext(),
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {hasEntriesInTable ? (
              table.getRowModel().rows.map((row) => (
                <TableRow
                  key={row.id}
                  data-state={row.getIsSelected() && 'selected'}
                >
                  {row.getVisibleCells().map((cell) => (
                    <TableCell key={cell.id}>
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext(),
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell
                  colSpan={table.getAllColumns().length}
                  className="h-64 text-center"
                >
                  <Loading />
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>

      <DataTablePagination
        table={table}
        dataQuery={dataQuery}
        disabled={!hasEntriesInTable}
      />
    </div>
  );
}
