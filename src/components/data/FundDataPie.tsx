import { Chain, Token } from '@/lib/debank/totalBalance/type';
import { valueFormat } from '@/lib/utils';
import { generateColorHex } from '@/utils/color/color';
import { ResponsivePie } from '@nivo/pie';

interface PieData {
  id: string;
  label: string;
  value: number;
  color: string;
}

const parseData = (data: Chain[] | Token[]) => {
  if ((data as Chain[])[0]?.usd_value) return parseChainData(data as Chain[]);
  else if ((data as Token[])[0]?.amount) return parseTokenData(data as Token[]);
  return [];
};

const parseChainData = (data: Chain[]): PieData[] => {
  return data.map((x: Chain): PieData => {
    return {
      id: x.id,
      label: x.name,
      value: x.usd_value,
      color: generateColorHex(x.name, x.community_id),
    };
  });
};

const parseTokenData = (data: Token[]): PieData[] => {
  return data.map((x: Token, i: number): PieData => {
    return {
      id: `${x.chain}/${x.name}`,
      label: `${x.chain}/${x.name}`,
      value: x.amount * x.price,
      color: generateColorHex(`${x.chain}/${x.name}`, i * 17),
    };
  });
};

interface FundDataPieProps {
  data: Chain[] | Token[];
  parseValue?: boolean;
}

const FundDataPie = ({ data, parseValue = true }: FundDataPieProps) => {
  return (
    <ResponsivePie
      valueFormat={valueFormat}
      data={parseData(data)}
      theme={{ background: '#D9F3E2' }}
      margin={{ top: 40, right: 60, bottom: 80, left: 40 }}
      innerRadius={0.5}
      padAngle={0.7}
      cornerRadius={3}
      activeOuterRadiusOffset={8}
      borderWidth={3}
      borderColor={{
        from: 'color',
        modifiers: [['darker', 0.2]],
      }}
      arcLinkLabelsSkipAngle={10}
      arcLinkLabelsTextColor="#232929"
      arcLinkLabelsThickness={2}
      arcLinkLabelsColor={{
        from: 'color',
        modifiers: [['darker', 2]],
      }}
      arcLabelsSkipAngle={10}
      colors={{ datum: 'data.color' }}
      arcLabelsTextColor={{
        from: 'color',
        modifiers: [['darker', 2]],
      }}
      defs={[]}
      fill={[]}
      legends={[
        {
          anchor: 'bottom-left',
          direction: 'column',
          justify: false,
          translateX: -20,
          translateY: 60,
          itemsSpacing: 3,
          itemWidth: 100,
          itemHeight: 18,
          itemTextColor: '#999',
          itemDirection: 'left-to-right',
          itemOpacity: 1,
          symbolSize: 18,
          symbolShape: 'circle',
          effects: [
            {
              on: 'hover',
              style: {
                itemTextColor: '#232929',
              },
            },
          ],
        },
      ]}
    />
  );
};

export default FundDataPie;
