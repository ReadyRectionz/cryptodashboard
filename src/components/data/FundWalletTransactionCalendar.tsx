import { ResponsiveTimeRange } from '@nivo/calendar';

const parseData = (data: { [key: string]: number }) => {
  return Object.entries(data).map((x) => ({
    value: x[1],
    day: x[0],
  }));
};

interface FundWalletTransactionCalendarProps {
  data: { [key: string]: number };
}

const FundWalletTransactionCalendar = ({
  data,
}: FundWalletTransactionCalendarProps) => {
  const now = new Date();
  const nowString = now.toISOString().slice(0, 10);
  var aboutAWeekAgo = new Date();
  aboutAWeekAgo.setDate(now.getDate() - 28);
  const aboutAWeekAgoString = aboutAWeekAgo.toISOString().slice(0, 10);

  return (
    <ResponsiveTimeRange
      data={parseData(data)}
      from={aboutAWeekAgoString}
      to={nowString}
      emptyColor="#eeeeee"
      colors={['#ecfff7', '#9dffd8', '#4fffb8', '#00b16a']}
      margin={{ top: 40, right: 10, bottom: 10, left: 10 }}
      dayBorderWidth={2}
      dayBorderColor="#ffffff"
      weekdayTicks={[0, 1, 2, 3, 4, 5, 6]}
      weekdayLegendOffset={70}
      legends={[
        {
          anchor: 'bottom-right',
          direction: 'column',
          justify: false,
          itemCount: 4,
          itemWidth: 42,
          itemHeight: 10,
          itemsSpacing: 14,
          itemDirection: 'right-to-left',
          translateX: -15,
          translateY: -45,
          symbolSize: 20,
        },
      ]}
    />
  );
};

export default FundWalletTransactionCalendar;
