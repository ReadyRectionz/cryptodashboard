'use client';
import { ColumnDef } from '@tanstack/react-table';
import {
  HistoryListItem,
  HistoryItemTx,
  WalletHistroyDicts,
} from '@/lib/debank/totalBalance/type';
import { DataTableColumnHeader } from '../data-table/data-table-column-header';
import Image from 'next/image';
import { mapTokenFromDict } from '../mapper/tokenMapper';

const FundWalletTransactionTableColumns = (
  dicts: WalletHistroyDicts,
): ColumnDef<HistoryListItem>[] => {
  const tokenDict = dicts.token_dict;
  const projectDict = dicts.project_dict;
  const cexDict = dicts.cex_dict;

  return [
    {
      accessorKey: 'chain',
      header: () => <div>Chain</div>,
      cell: ({ row }) => {
        const chain: string = row.original.chain;

        return <div className="font-medium">{chain}</div>;
      },
    },

    {
      accessorKey: 'tx',
      header: () => (
        <div>
          Type <span className="text-xs">(Selector)</span>
        </div>
      ),
      cell: ({ row }) => {
        const tx: HistoryItemTx | null = row.original.tx;
        const name = tx?.['name'] || '';
        const selector = tx?.['selector'] || '';

        const projectId: string | null = row.original.project_id;

        const project = projectDict?.[projectId ? projectId : ''];

        return (
          <div className="flex flex-col">
            <div className="flex gap-2">
              {project?.logo_url && (
                <Image
                  src={project?.logo_url}
                  width={20}
                  height={20}
                  alt="logo"
                />
              )}
              <p className="font-medium">
                {project?.name}
                {/* {project.id, project.site_url} */}(
                <span className="font-normal">{name}</span>)
              </p>
            </div>
            <p className="text-xs w-20 truncate">{selector}</p>
          </div>
        );
      },
    },
    {
      accessorKey: 'time_at',
      header: ({ column }) => {
        return <DataTableColumnHeader column={column} title="Date" />;
      },
      cell: ({ row }) => {
        const timeAt: number = row.original.time_at;
        const timestamp = timeAt * 1000;
        const formatted = new Intl.DateTimeFormat('nl-NL', {
          day: 'numeric',
          month: 'short',
          year: 'numeric',
        }).format(timestamp);

        return <div className="font-medium">{formatted}</div>;
      },
    },
    {
      accessorKey: 'receives',
      header: () => (
        <div>
          <p>Receive(s)</p>
          <p className="text-xs">(token_id, from_addr, amount)</p>
        </div>
      ),
      cell: ({ row }) => {
        const receives:
          | {
              amount: number;
              from_addr: string;
              token_id: string;
            }[]
          | null = row.original.receives || null;

        return (
          <div className="flex flex-col gap-5">
            {receives?.map((x, idx) => {
              const token = tokenDict?.[x.token_id];

              return mapTokenFromDict(token, x.from_addr, x.amount, idx);
            })}
          </div>
        );
      },
    },
    {
      accessorKey: 'sends',
      header: () => (
        <div>
          <p>Send(s)</p>
          <p className="text-xs">(token_id, to_addr, amount)</p>
        </div>
      ),
      cell: ({ row }) => {
        const sends:
          | {
              amount: number;
              to_addr: string;
              token_id: string;
            }[]
          | null = row.original.sends || null;

        return (
          <div className="flex flex-col gap-5">
            {sends?.map((x, idx) => {
              const token = tokenDict?.[x.token_id];

              return mapTokenFromDict(token, x.to_addr, x.amount, idx);
            })}
          </div>
        );
      },
    },
    {
      accessorKey: 'approve',
      header: () => (
        <div>
          <p>Approve</p>
          <p className="text-xs">(token_id, spender, amount)</p>
        </div>
      ),
      cell: ({ row }) => {
        const approve: {
          spender: string;
          token_id: string;
          value: number;
        } | null = row.original.token_approve;

        if (approve) {
          const token = tokenDict?.[approve?.token_id];

          return (
            <div className="flex flex-col gap-5">
              {true &&
                mapTokenFromDict(token, approve.spender, approve.value, 0)}
            </div>
          );
        }
      },
    },

    {
      accessorKey: 'is_scam',
      header: () => <div>Scam?</div>,
      cell: ({ row }) => {
        const scam: boolean = row.original.is_scam;

        return <div className="font-medium">{scam ? 'Yes' : 'No'}</div>;
      },
    },
  ];
};

export default FundWalletTransactionTableColumns;
