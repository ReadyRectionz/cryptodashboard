import { WalletNetCurveOverview, WalletSnapshotOverview } from '@/actions/type';
import { valueFormat } from '@/lib/utils';
import { generateColorHex } from '@/utils/color/color';
import { ResponsiveLine, SliceTooltipProps } from '@nivo/line';

interface LineData {
  id: string;
  label: string;
  color: string;
  data: {
    x: Date;
    y: number;
  }[];
}

const toolTipFunction = ({ slice }: SliceTooltipProps) => {
  let total = 0;
  return (
    <div className="bg-white text-inherit rounder-sm shadow-white shadow-sm px-[5px] py-[9px]">
      <div>
        <p>
          Date{' '}
          <span className="font-bold">{slice.points[0].data.xFormatted}</span>
        </p>
        <table className="w-full border-collapse">
          <tbody>
            {slice.points.map((x, i) => {
              total += parseFloat(x.data.y.toString());
              return (
                <tr key={i}>
                  <td className="px-[3px] py-[5px]">
                    <span
                      className="block w-3 h-3 mr-[7px]"
                      style={{ backgroundColor: x.borderColor }}
                    />
                  </td>
                  <td className="px-[3px] py-[5px]">{x.id.split('.')[0]}</td>
                  <td className="px-[3px] py-[5px]">
                    <span className="font-bold">{x.data.yFormatted}</span>
                  </td>
                </tr>
              );
            })}
            <tr className="border-solid border-t-2 border-black">
              <td className="px-[3px] py-[5px]">
                <span className={`block w-3 h-3 bg-black mr-[7px]`} />
              </td>
              <td className="px-[3px] py-[5px]">Total</td>
              <td className="px-[3px] py-[5px]">
                <span className="font-bold">{valueFormat(total)}</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

const parseData = (
  data: WalletNetCurveOverview[] | WalletSnapshotOverview[],
) => {
  if ((data as WalletSnapshotOverview[])[0]?.entries[0].time)
    return parseSnapshotData(data as WalletSnapshotOverview[]);
  else if ((data as WalletNetCurveOverview[])[0]?.entries[0].timestamp)
    return parseNetcurveData(data as WalletNetCurveOverview[]);
  return [];
};

const parseSnapshotData = (data: WalletSnapshotOverview[]): LineData[] => {
  return data.map((x: WalletSnapshotOverview): LineData => {
    return {
      id: x.name,
      label: x.name,
      color: generateColorHex(x.name, x.id),
      data: x.entries.map((d: any) => ({
        x: new Date(new Date(d.time).toLocaleDateString()),
        y: d.value,
      })),
    };
  });
};

const parseNetcurveData = (data: WalletNetCurveOverview[]): LineData[] => {
  return data.map((x: WalletNetCurveOverview, i: number): LineData => {
    return {
      id: x.name,
      label: x.name,
      color: generateColorHex(x.name, x.id),
      data: x.entries.map((d: any) => ({
        x: new Date(d.timestamp * 1000),
        y: d.usd_value,
      })),
    };
  });
};

interface FundDataLineProps {
  data: WalletNetCurveOverview[] | WalletSnapshotOverview[];
  options?: {
    stacked?: boolean;
    xFormat?: string;
  };
}

const FundDataLine = ({ data, options }: FundDataLineProps) => {
  return (
    <ResponsiveLine
      data={parseData(data)}
      theme={{ background: '#D9F3E2' }}
      margin={{ top: 50, right: 160, bottom: 80, left: 100 }}
      xScale={{
        format: options?.xFormat || '%d-%m-%Y %H:%M',
        precision: 'minute',
        type: 'time',
        useUTC: false,
      }}
      yScale={{
        type: 'linear',
        min: 'auto',
        max: 'auto',
        stacked: options?.stacked || true,
        reverse: false,
      }}
      yFormat={(value) => {
        return valueFormat(parseFloat(value.toString()));
      }}
      xFormat={`time:${options?.xFormat || '%d-%m-%Y %H:%M'}`}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 40,
        legend: 'time',
        legendOffset: 36,
        legendPosition: 'start',
        truncateTickAt: 0,
        format: options?.xFormat || '%d-%m-%Y %H:%M',
      }}
      axisLeft={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: 'value',
        legendOffset: -80,
        legendPosition: 'middle',
        truncateTickAt: 0,
        format: valueFormat,
      }}
      pointSize={10}
      pointColor={{ theme: 'background' }}
      pointBorderWidth={2}
      pointBorderColor={{ from: 'serieColor' }}
      pointLabelYOffset={-12}
      curve="linear"
      sliceTooltip={toolTipFunction}
      areaOpacity={0}
      areaBlendMode="normal"
      areaBaselineValue={0}
      enableTouchCrosshair={true}
      enableSlices="x"
      enableGridX={true}
      enableGridY={true}
      enablePoints={true}
      enablePointLabel={false}
      enableArea={false}
      enableCrosshair={true}
      lineWidth={2}
      colors={{ scheme: 'nivo' }}
      isInteractive={true}
      useMesh={true}
      debugMesh={false}
      debugSlices={false}
      crosshairType="x"
      layers={[
        'grid',
        'markers',
        'axes',
        'areas',
        'crosshair',
        'lines',
        'points',
        'slices',
        'mesh',
        'legends',
      ]}
      pointLabel="x"
      tooltip={(x) => {
        return <></>;
      }}
      role=""
      fill={[]}
      defs={[]}
      legends={[
        {
          anchor: 'bottom-right',
          direction: 'column',
          justify: false,
          translateX: 100,
          translateY: 0,
          itemsSpacing: 0,
          itemDirection: 'left-to-right',
          itemWidth: 80,
          itemHeight: 20,
          itemOpacity: 0.75,
          symbolSize: 12,
          symbolShape: 'circle',
          symbolBorderColor: 'rgba(0, 0, 0, .5)',
          effects: [
            {
              on: 'hover',
              style: {
                itemBackground: 'rgba(0, 0, 0, .03)',
                itemOpacity: 1,
              },
            },
          ],
        },
      ]}
    />
  );
};

export default FundDataLine;
