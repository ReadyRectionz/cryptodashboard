import { fetchPoolChainBalances, fetchPoolTokenBalances } from '@/actions/fund';
import {
  FundWithWalletExtended,
  TotalChainBalance,
  TotalTokenBalance,
} from '@/actions/type';
import { useState, useEffect } from 'react';
import { ToggleGroup, ToggleGroupItem } from '../ui/toggle-group';
import { CoinsIcon, LinkIcon } from 'lucide-react';
import { valueFormat } from '@/lib/utils';
import Loading from '@/app/loading';
import { useDeepEqualMemo } from '@/utils/deepmemo';
import FundDataPie from '../data/FundDataPie';

interface FundChainTokenBalanceCardProps {
  fund?: FundWithWalletExtended;
  showPlaceHolder: boolean;
}

const FundChainTokenBalanceCard = ({
  fund,
  showPlaceHolder,
}: FundChainTokenBalanceCardProps) => {
  const [loading, setLoading] = useState(true);

  const [viewValue, setViewValue] = useState('chain');

  const [chainBalances, setChainBalances] = useState<TotalChainBalance>();
  const [tokenBalances, setTokenBalances] = useState<TotalTokenBalance>();

  const comparableFund = useDeepEqualMemo(fund);
  const compTb = useDeepEqualMemo(tokenBalances);
  const compCb = useDeepEqualMemo(chainBalances);

  const chainData = Object.values(
    (viewValue === 'chain'
      ? chainBalances?.chain_object
      : tokenBalances?.token_object) || {},
  );

  const usdValue =
    viewValue === 'chain'
      ? chainBalances?.total_usd_value
      : tokenBalances?.total_usd_value;

  const isLoading = loading || showPlaceHolder;
  const hasData = chainData && chainData?.length > 0;

  useEffect(() => {
    const fetchChainData = async (fund: FundWithWalletExtended) => {
      const { data, error } = await fetchPoolChainBalances(fund);

      if (error || !data) {
        // seterr(data);
        return;
      }

      setChainBalances(data);
    };

    const fetchTokenData = async (fund: FundWithWalletExtended) => {
      const { data, error } = await fetchPoolTokenBalances(fund);

      if (error || !data) {
        // seterr(data);
        return;
      }

      setTokenBalances(data);
    };

    const isReadyForFetching = comparableFund && !showPlaceHolder;
    const shouldFetchChain = viewValue === 'chain' && !compCb;
    const shouldFetchToken = viewValue === 'token' && !compTb;

    if (isReadyForFetching && shouldFetchChain) {
      setLoading(true);

      fetchChainData(comparableFund).finally(() => {
        setLoading(false);
      });
    } else if (isReadyForFetching && shouldFetchToken) {
      setLoading(true);

      fetchTokenData(comparableFund).finally(() => {
        setLoading(false);
      });
    }
  }, [viewValue, comparableFund, compTb, compCb, showPlaceHolder]);

  return (
    <div className="group relative flex min-h-[420px] w-full max-w-[550px] flex-col overflow-hidden rounded-xl bg-white shadow-empty transition-all hover:shadow-lg md:min-h-[428px]">
      {isLoading ? (
        <Loading />
      ) : !hasData ? (
        <div className="flex-center flex-grow  bg-gray-50 bg-cover bg-center text-grey-500">
          <p className="text-4xl font-extrabold text-center p-20"> No data</p>
        </div>
      ) : (
        <FundDataPie data={chainData} />
      )}
      <div className="absolute right-2 top-2 flex flex-col gap-4 rounded-xl bg-white p-3  shadow-sm transition-full">
        <ToggleGroup
          type="single"
          disabled={isLoading}
          defaultValue={viewValue}
          onValueChange={(value) => {
            if (value) setViewValue(value);
          }}
        >
          <ToggleGroupItem value="chain">
            <LinkIcon />
          </ToggleGroupItem>
          <ToggleGroupItem value="token">
            <CoinsIcon />
          </ToggleGroupItem>
        </ToggleGroup>
      </div>

      <div className="absolute right-2 bottom-2 flex flex-col gap-4 rounded-xl bg-white p-3  shadow-sm transition-full">
        <p>
          Total USD Value:{' '}
          <span className="font-semibold">
            {isLoading ? 'Loading...' : valueFormat(usdValue)}
          </span>
        </p>
      </div>
    </div>
  );
};

export default FundChainTokenBalanceCard;
