import { deleteWallet } from '@/lib/database/wallet/actions';
import { WalletDBExtended } from '@/lib/database/wallet/type';
import { fetchWalletHistorySumarized } from '@/lib/debank/totalBalance/actions';
import Image from 'next/image';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { useDeepEqualMemo } from '@/utils/deepmemo';
import FundWalletTransactionCalendar from '../data/FundWalletTransactionCalendar';

interface WalletOverviewCardProps {
  wallet: WalletDBExtended;
  refreshWallets: () => void;
}

const FundWalletOverviewCard = ({
  wallet,
  refreshWallets,
}: WalletOverviewCardProps) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<{ [key: string]: number }>();

  const cWallet = useDeepEqualMemo(wallet);

  const hasData = data && Object.keys(data).length > 0;

  useEffect(() => {
    setLoading(true);

    const fetchSumarizedHistory = async (wallet?: WalletDBExtended) => {
      if (wallet && wallet?.pubkey) {
        const history = await fetchWalletHistorySumarized(wallet.pubkey);
        setData(history);
      }
    };

    fetchSumarizedHistory(cWallet).finally(() => {
      setLoading(false);
    });
  }, [cWallet]);

  return (
    <>
      {loading ? (
        <div className="flex flex-col flex-grow h-full w-full items-center justify-center bg-gray-50 bg-cover bg-center text-grey-500 ">
          <Image
            src="/assets/icons/spinner.svg"
            alt="spinner"
            width={100}
            height={100}
          />
          <p className="text-xl font-bold">Loading...</p>
        </div>
      ) : !hasData ? (
        <div className="flex-center flex-grow bg-gray-50 bg-cover bg-center text-grey-500">
          <p className="text-4xl font-extrabold text-center p-20"> No data</p>
        </div>
      ) : (
        <>
          <div className="flex flex-col max-h-6">
            <span>Number of transactions</span>
            <span className="text-xs">Last 4 weeks</span>
          </div>
          <FundWalletTransactionCalendar data={data} />
        </>
      )}
      <div className="absolute right-2 top-2 flex flex-col gap-4 rounded-xl bg-white p-3 shadow-sm transition-full">
        <Link href={`/wallet/${wallet.pubkey_id}/edit`}>
          <Image
            src="/assets/icons/edit.svg"
            alt="edit"
            width={20}
            height={20}
          />
        </Link>
        <div
          onClick={() => {
            deleteWallet(wallet.id);
            refreshWallets();
          }}
        >
          <Image
            src="/assets/icons/delete.svg"
            alt="delete"
            width={20}
            height={20}
          />
        </div>
      </div>

      <div className="flex min-h-24 flex-col gap-3 p-1 md:gap-4">
        <Link href={`wallet/${wallet.pubkey_id}`}>
          <p className="text-sm font-semibold flex-1 text-black">
            {wallet.name}
          </p>
        </Link>
        <div className="flex gap-2">
          <span
            className={`font-medium w-min rounded-full ${wallet.active ? 'bg-green-100' : 'bg-red-100'} px-4 py-1 text-green-60`}
          >
            {wallet.active ? 'active' : 'disabled'}
          </span>
          <span className="font-medium w-24 rounded-full bg-gray-50 px-4 py-1 text-grey-500 truncate">
            {wallet.pubkey}
          </span>
        </div>
      </div>
    </>
  );
};
export default FundWalletOverviewCard;
