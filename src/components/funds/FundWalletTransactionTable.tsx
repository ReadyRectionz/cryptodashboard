import { WalletDBExtended } from '@/lib/database/wallet/type';
import { fetchWalletHistory } from '@/lib/debank/totalBalance/actions';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { PaginationState } from '@tanstack/react-table';
import { useState } from 'react';
import { DataTable } from '../data-table/data-table';
import FundWalletTransactionTableColumns from '../data/FundWalletTransactionTableColumns';

interface FundWalletTransactionTableProps {
  wallet: WalletDBExtended;
}

interface TimevalPagination {
  prev: number;
  curr: number;
  next: number;
  page: number;
}

const FundWalletTransactionTable = ({
  wallet,
}: FundWalletTransactionTableProps) => {
  const [timevalPage, setTimevalPage] = useState<TimevalPagination>({
    prev: 0,
    curr: 0,
    next: 0,
    page: 0,
  });

  const dataTableFetchWalletHistory = async (pagination: PaginationState) => {
    const paginatedWalletHistoryFetchFunction = async (
      options: PaginationState,
      timeval: TimevalPagination,
    ) => {
      const startTime =
        options.pageIndex == 0
          ? 0
          : options.pageIndex > timeval.page
            ? timeval.next
            : timeval.prev;

      const response = await fetchWalletHistory(
        wallet.pubkey ?? '',
        options.pageSize,
        startTime,
      );

      return {
        rows: response.history_list,
        dicts: {
          cex_dict: response.cex_dict,
          project_dict: response.project_dict,
          cate_dict: response.cate_dict,
          token_dict: response.token_dict,
        },
        curr: response.history_list[0].time_at,
        next: response.history_list[response.history_list.length - 1].time_at,
      };
    };

    const data = await paginatedWalletHistoryFetchFunction(
      pagination,
      timevalPage,
    );
    setTimevalPage({
      prev: data.curr < timevalPage.curr ? timevalPage.curr : 0,
      curr: data.curr,
      next: data.next,
      page: pagination.pageIndex,
    });

    return data;
  };

  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <DataTable
        fetchData={dataTableFetchWalletHistory}
        fetchColumns={FundWalletTransactionTableColumns}
      />
    </QueryClientProvider>
  );
};

export default FundWalletTransactionTable;
