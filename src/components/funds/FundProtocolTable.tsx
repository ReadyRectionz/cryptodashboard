import { fetchPoolProtocols } from '@/actions/fund';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { WalletDBExtended } from '@/lib/database/wallet/type';
import { ComplexProtocol } from '@/lib/debank/totalBalance/type';
import { valueFormat } from '@/lib/utils';
import { CopyIcon } from 'lucide-react';
import Image from 'next/image';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { mapToken } from '../mapper/tokenMapper';
import Loading from '@/app/loading';
import { useDeepEqualMemo } from '@/utils/deepmemo';

interface FundProtocolTableProps {
  wallets?: WalletDBExtended[];
  showPlaceHolder: boolean;
}

const FundProtocolTable = ({
  wallets,
  showPlaceHolder,
}: FundProtocolTableProps) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<any>();
  const [protocols, setProtocols] = useState<ComplexProtocol[]>();

  const compWallets = useDeepEqualMemo(wallets);

  const isLoading = loading || showPlaceHolder;
  const hasData = protocols && protocols?.length > 0;

  useEffect(() => {
    setLoading(true);

    const fetchData = async () => {
      if (compWallets && !showPlaceHolder) {
        const { data, error } = await fetchPoolProtocols(...compWallets);

        if (error || !data) {
          setError(error);
          return;
        }

        setProtocols(data);
      }
    };

    fetchData().finally(() => {
      setLoading(false);
    });
  }, [compWallets, showPlaceHolder]);

  return (
    <div
      className={`rounded-xl ${isLoading || !hasData ? 'bg-gray-50' : 'bg-[#D9F3E2]'} p-5`}
    >
      <Table>
        <TableHeader>
          <TableRow>
            <TableHead>Provider</TableHead>
            <TableHead className="w-[80px]">Chain</TableHead>
            <TableHead className="w-[100px]">Type</TableHead>
            <TableHead>
              Tokens
              <div className="flex gap-6">
                <div className="flex-1 text-xs">Supply</div>
                <div className="flex-1 text-xs">Reward</div>
                <div className="flex-1 text-xs">Borrow</div>
              </div>
            </TableHead>
            <TableHead className="w-[100px]">TVL</TableHead>
            <TableHead className="w-[100px]">Pool</TableHead>
            <TableHead />
            <TableHead className="text-right">Amount</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          {isLoading ? (
            <TableRow>
              <TableCell colSpan={8} className="h-64 text-center">
                <Loading />
              </TableCell>
            </TableRow>
          ) : !hasData ? (
            <TableRow>
              <TableCell colSpan={8} className="h-64 text-center">
                <p className="text-4xl font-extrabold text-center p-20">
                  No data
                </p>
              </TableCell>
            </TableRow>
          ) : (
            protocols.map((p) => {
              return p.portfolio_item_list.map((pi, idx2: number) => {
                return (
                  <TableRow key={idx2}>
                    <TableCell className="font-medium">
                      <div className="flex gap-3">
                        <Image
                          src={p.logo_url}
                          width={20}
                          height={20}
                          alt="icon"
                        />
                        <Link href={p.site_url} className="line-clamp-1">
                          {p.name}
                        </Link>
                      </div>
                    </TableCell>
                    <TableCell className="font-medium">{p.chain}</TableCell>
                    <TableCell>{pi.name}</TableCell>
                    <TableCell>
                      <div className="flex">
                        <div className="flex flex-1 flex-col gap-3">
                          {(pi.detail?.supply_token_list || []).map(mapToken)}
                        </div>
                        <div className="flex-1">
                          {(pi.detail?.reward_token_list || []).map(mapToken)}
                        </div>
                        <div className="flex-1">
                          {(pi.detail?.borrow_token_list || []).map(mapToken)}
                        </div>
                      </div>
                    </TableCell>
                    <TableCell>{valueFormat(p.tvl)}</TableCell>
                    <TableCell className="truncate max-w-3">
                      {pi.pool?.id || 'Unkown'}
                    </TableCell>

                    <TableCell>
                      <CopyIcon
                        className="cursor-pointer"
                        onClick={() =>
                          navigator.clipboard.writeText(pi.pool?.id || '')
                        }
                      />
                    </TableCell>
                    <TableCell className="text-right">
                      {valueFormat(pi.stats.net_usd_value)}
                    </TableCell>
                  </TableRow>
                );
              });
            })
          )}
        </TableBody>
      </Table>
    </div>
  );
};

export default FundProtocolTable;
