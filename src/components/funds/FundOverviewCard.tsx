'use client';
import { FundWithWalletExtended } from '@/actions/type';
import { Chain } from '@/lib/debank/totalBalance/type';

import Link from 'next/link';

import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { valueFormat } from '@/lib/utils';
import { useDeepEqualMemo } from '@/utils/deepmemo';
import FundDataPie from '../data/FundDataPie';

interface FundOverviewCardProps {
  fund: FundWithWalletExtended;
}

const FundOverviewCard = ({ fund }: FundOverviewCardProps) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<Chain[]>([]);

  const comparableFund = useDeepEqualMemo(fund);

  const hasData = data && data.length > 0;

  useEffect(() => {
    if (comparableFund && comparableFund.total) {
      const dataInArray = Object.values(comparableFund.total.chain_object);
      setData(dataInArray);
    }
    setLoading(false);
  }, [comparableFund]);

  return (
    <div className="group relative flex min-h-[520px] w-full max-w-[600px] flex-col overflow-hidden rounded-xl bg-white shadow-empty transition-all hover:shadow-lg md:min-h-[488px]">
      {loading ? (
        <div className="flex-center flex-grow bg-gray-50 bg-cover bg-center text-grey-500">
          <p> Loading</p>
        </div>
      ) : !hasData ? (
        <div className="flex-center flex-grow  bg-gray-50 bg-cover bg-center text-grey-500">
          <p className="text-4xl font-extrabold text-center p-20"> No data</p>
        </div>
      ) : (
        <FundDataPie data={data} />
      )}
      <div className="absolute right-2 top-2 flex flex-col gap-4 rounded-xl bg-white p-3 shadow-sm transition-full">
        <Link href={`/funds/${fund.uuid}/update`}>
          <Image
            src="/assets/icons/edit.svg"
            alt="edit"
            width={20}
            height={20}
          />
        </Link>
        <Link href={`/funds/${fund.uuid}/delete`}>
          <Image
            src="/assets/icons/delete.svg"
            alt="delete"
            width={20}
            height={20}
          />
        </Link>
      </div>

      <div className="absolute right-2 bottom-56 flex flex-col gap-4 rounded-xl bg-white p-3 shadow-sm transition-full">
        <p>
          Total USD Value:{' '}
          <span className="font-semibold">
            {valueFormat(fund.total?.total_usd_value)}
          </span>
        </p>
      </div>

      <div className="flex min-h-[220px] max-h-[230px] flex-col gap-3 p-5 md:gap-4">
        <div className="flex gap-2">
          <span className="font-medium w-min rounded-full bg-green-100 px-4 py-1 text-green-60">
            {valueFormat(fund.min_amount)}
          </span>
          <p className="font-medium w-min rounded-full bg-gray-50 px-4 py-1 text-grey-500 line-clamp-1">
            {fund.uuid}
          </p>
        </div>
        <p className="text-sm text-grey-500 italic line-clamp-1 truncate">
          Created at: {new Date(fund.created_at).toLocaleDateString('nl-NL')}
        </p>
        <Link href={`funds/${fund.uuid}`}>
          <p className="text-lg font-semibold flex-1 text-black">{fund.name}</p>
        </Link>
        <div className="flex-between w-full">
          <p className="p-medium-14 md:p-medium-16 text-grey-600 line-clamp-2">
            {fund.description}
          </p>
        </div>
      </div>
    </div>
  );
};

export default FundOverviewCard;
