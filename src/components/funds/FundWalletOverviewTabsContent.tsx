import { WalletDBExtended } from '@/lib/database/wallet/type';
import { TabsContent } from '../ui/tabs';
import FundWalletTransactionTable from './FundWalletTransactionTable';

interface FundWalletOverviewTabsContentProps {
  wallets: WalletDBExtended[];
}

const FundWalletOverviewTabsContent = ({
  wallets,
}: FundWalletOverviewTabsContentProps) => {
  const hasData = wallets.length > 0;
  return (
    <>
      <p className="text-2xl font-bold py-5">Wallet Transaction Overview</p>

      {!hasData ? (
        <p className="text-4xl font-extrabold text-center p-20">No data</p>
      ) : (
        <>
          {wallets.map((x) => (
            <TabsContent key={x.id} value={x.id.toString()}>
              <FundWalletTransactionTable wallet={x} />
            </TabsContent>
          ))}
          <div className="flex justify-center">
            <p className="text-lg italic p-5">
              Select A wallet to show its transaction history
            </p>
          </div>
        </>
      )}
    </>
  );
};

export default FundWalletOverviewTabsContent;
