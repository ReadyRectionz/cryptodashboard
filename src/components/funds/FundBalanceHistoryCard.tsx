import {
  FundWithWalletExtended,
  WalletNetCurveOverview,
  WalletSnapshotOverview,
} from '@/actions/type';
import { useEffect, useState } from 'react';
import { ToggleGroup, ToggleGroupItem } from '../ui/toggle-group';
import {
  CalendarDaysIcon,
  Clock7Icon,
  TrendingDownIcon,
  TrendingUpIcon,
} from 'lucide-react';
import FundDataLine from '../data/FundDataLine';
import { fetchPoolNetCurve, fetchPoolSnapshots } from '@/actions/fund';
import { valueFormat } from '@/lib/utils';
import Loading from '@/app/loading';
import { useDeepEqualMemo } from '@/utils/deepmemo';

interface FundBalanceHistoryCardProps {
  fund?: FundWithWalletExtended;
  showPlaceHolder: boolean;
}

const calculateDiffernces = (
  data: WalletNetCurveOverview[] | WalletSnapshotOverview[],
) => {
  let moneyAtT0 = 0;
  let moneyAtTend = 0;
  for (let w of data) {
    moneyAtT0 += (w as WalletSnapshotOverview)?.entries[0].time
      ? (w as WalletSnapshotOverview).entries[0].value
      : (w as WalletNetCurveOverview)?.entries[0].usd_value;
    moneyAtTend += (w as WalletSnapshotOverview)?.entries[w.entries.length - 1]
      .time
      ? (w as WalletSnapshotOverview).entries[w.entries.length - 1].value
      : (w as WalletNetCurveOverview)?.entries[w.entries.length - 1].usd_value;
  }

  return moneyAtTend - moneyAtT0;
};

const FundBalanceHistoryCard = ({
  fund,
  showPlaceHolder,
}: FundBalanceHistoryCardProps) => {
  const [loading, setLoading] = useState(false);

  const [viewValue, setViewValue] = useState('year');

  const [dayHistory, setDayHistory] = useState<WalletNetCurveOverview[]>();
  const [dayDifference, setDayDifference] = useState(0);
  const [yearHistory, setYearHistory] = useState<WalletSnapshotOverview[]>();
  const [yearDifference, setYearDifference] = useState(0);

  const comparableFund = useDeepEqualMemo(fund);
  const compDh = useDeepEqualMemo(dayHistory);
  const compYh = useDeepEqualMemo(yearHistory);

  const viewData = viewValue === 'day' ? dayHistory : yearHistory;
  const viewOpt = viewValue === 'year' ? { xFormat: '%d-%m-%Y' } : undefined;
  const usdDiff = viewValue === 'day' ? dayDifference : yearDifference;

  const isLoading = loading || showPlaceHolder;
  const hasData = viewData && viewData?.length > 0;
  const isNegative = usdDiff < 0;
  const isPositive = usdDiff > 0;

  useEffect(() => {
    const fetchDayData = async (fund: FundWithWalletExtended) => {
      const { data, error } = await fetchPoolNetCurve(...fund.wallet);

      if (error || !data) {
        // seterr(data);
        return;
      }

      setDayHistory(data);
      setDayDifference(calculateDiffernces(data));
    };
    const fetchYearData = async (fund: FundWithWalletExtended) => {
      const { data, error } = await fetchPoolSnapshots(...fund.wallet);
      if (error || !data) {
        // seterr(data);
        return;
      }
      setYearHistory(data);
      setYearDifference(-1 * calculateDiffernces(data));
    };

    if (comparableFund && viewValue === 'day' && !compDh) {
      setLoading(true);

      fetchDayData(comparableFund).finally(() => {
        setLoading(false);
      });
    } else if (comparableFund && viewValue === 'year' && !compYh) {
      setLoading(true);

      fetchYearData(comparableFund).finally(() => {
        setLoading(false);
      });
    }
  }, [viewValue, compDh, compYh, comparableFund]);

  return (
    <div className="group relative flex min-h-[520px] w-full flex-col overflow-hidden rounded-xl bg-white shadow-empty transition-all hover:shadow-lg md:min-h-[488px]">
      {isLoading ? (
        <Loading />
      ) : !hasData ? (
        <div className="flex-center flex-grow  bg-gray-50 bg-cover bg-center text-grey-500">
          <p className="text-4xl font-extrabold text-center p-20"> No data</p>
        </div>
      ) : (
        <FundDataLine data={viewData || []} options={viewOpt} />
      )}
      <div className="absolute right-2 top-2 flex flex-col gap-4 rounded-xl bg-white p-3  shadow-sm transition-full">
        <ToggleGroup
          type="single"
          disabled={isLoading}
          defaultValue={'year'}
          onValueChange={(value) => {
            if (value) setViewValue(value);
          }}
        >
          <ToggleGroupItem value="day">
            <Clock7Icon />
          </ToggleGroupItem>
          <ToggleGroupItem value="year">
            <CalendarDaysIcon />
          </ToggleGroupItem>
        </ToggleGroup>
      </div>

      <div className="absolute right-2 bottom-2 flex flex-col gap-4 rounded-xl bg-white p-3 w-max shadow-sm transition-full">
        <p className="clamp-line-1 flex flex-row gap-2">
          Total Difference:{' '}
          <span className="font-semibold flex flex-row gap-1">
            {isLoading ? (
              'Loading... '
            ) : (
              <>
                <span>
                  {isNegative && '-'}
                  {valueFormat(Math.abs(usdDiff))}
                </span>
                <span>
                  {isNegative ? (
                    <TrendingDownIcon />
                  ) : isPositive ? (
                    <TrendingUpIcon />
                  ) : (
                    ''
                  )}
                </span>
              </>
            )}
          </span>
        </p>
      </div>
    </div>
  );
};

export default FundBalanceHistoryCard;
