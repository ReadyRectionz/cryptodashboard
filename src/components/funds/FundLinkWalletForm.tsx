'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

import { Button } from '@/components/ui/button';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';

import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from '@/components/ui/card';
import { Input } from '@/components/ui/input';
import { isAddress } from 'web3-validator';
import { useState } from 'react';
import { insertWallet } from '@/lib/database/wallet/actions';

const formSchema = z.object({
  pubkey: z
    .string()
    .refine(
      (value) => /^0x[a-fA-F0-9]{40}$/g.test(value ?? ''),
      'Public key should be a hexstring of 40 characters long',
    )
    .refine(
      (value) => isAddress(value ?? ''),
      'Public key should be a valid address',
    ),
  name: z.string().min(1, {
    message: 'Name cannot be empty.',
  }),
});

interface LinkWalletFormProps {
  fund_id: number;
  refreshWallets: () => void;
}

const FundLinkWalletForm = ({
  fund_id,
  refreshWallets,
}: LinkWalletFormProps) => {
  const fundDbId = fund_id;
  const [error, setError] = useState<any>();

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      pubkey: '',
      name: '',
    },
  });

  const handleLink = async (
    fund_id: any,
    values: { pubkey: string; name: string },
  ) => {
    const { pubkey, name } = values;
    await insertWallet(fund_id, pubkey, name);
  };

  async function onSubmit(values: z.infer<typeof formSchema>) {
    await handleLink(fundDbId, values);
    refreshWallets();
  }

  return (
    <Card className="w-[350px] ">
      <CardHeader>
        <CardTitle>Link wallet to Poolfolio</CardTitle>
        <CardDescription>
          Link a new wallet to Poolfolio in one-click.
        </CardDescription>
      </CardHeader>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
          <CardContent>
            <FormField
              control={form.control}
              name="pubkey"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Public Key</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Pub. key of wallet: 0xABCDEF1234...."
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Name</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Name of wallet: PLA Optimism HOT SC"
                      type="name"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </CardContent>
          <CardFooter className="flex justify-between">
            <Button variant="outline">Cancel</Button>
            <Button>Link</Button>
          </CardFooter>
        </form>
      </Form>
    </Card>
  );
};

export default FundLinkWalletForm;
