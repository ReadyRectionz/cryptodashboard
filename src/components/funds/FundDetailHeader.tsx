import { valueFormat } from '@/lib/utils';
import { Skeleton } from '../ui/skeleton';
import { FundWithWalletExtended } from '@/actions/type';

interface FundDetailHeaderProps {
  fund?: FundWithWalletExtended;
  totalBalance: number;
  showPlaceHolder: boolean;
}

const FundDetailHeader = ({
  fund,
  totalBalance,
  showPlaceHolder,
}: FundDetailHeaderProps) => {
  return (
    <div className="flex bg-gray-50 px-20 pt-10 pb-4">
      <div className="flex-1">
        {showPlaceHolder ? (
          <>
            <Skeleton className="w-[200px] h-[40px]" />
            <Skeleton className="w-[100px] h-[20px] mt-2" />
          </>
        ) : fund ? (
          <>
            <p className="text-4xl font-medium">{fund.name}</p>
            <p className="text-lg">{fund.description}</p>
          </>
        ) : (
          <p className="text-lg italic">Could not fetch fund</p>
        )}
      </div>
      <div className="flex-initial w-64">
        <p className="text-sm">Min amount</p>
        {showPlaceHolder ? (
          <Skeleton className="w-[100px] h-[40px] mt-4" />
        ) : fund ? (
          <>
            <p className="text-2xl pt-4">{valueFormat(fund.min_amount)}</p>
          </>
        ) : (
          <p className="text-lg italic">--</p>
        )}
      </div>
      <div className="flex-initial w-32">
        <p className="text-sm">Total USD Value</p>

        {showPlaceHolder ? (
          <Skeleton className="w-[100px] h-[40px] mt-4" />
        ) : (
          <p className="text-2xl pt-4">{valueFormat(totalBalance)}</p>
        )}
      </div>
    </div>
  );
};

export default FundDetailHeader;
