import { TabsList, TabsTrigger } from '../ui/tabs';
import { WalletDBExtended } from '@/lib/database/wallet/type';
import { ScrollArea, ScrollBar } from '../ui/scroll-area';
import FundWalletOverviewCard from './FundWalletOverviewCard';

interface FundWalletOverviewTabsListProps {
  wallets: WalletDBExtended[];
  refreshWallets: () => void;
}

const FundWalletOverviewTabsList = ({
  wallets,
  refreshWallets,
}: FundWalletOverviewTabsListProps) => {
  const hasData = wallets.length > 0;
  return (
    <ScrollArea className="max-w-[1420px] whitespace-nowrap rounded-md border">
      <TabsList className="flex w-full space-x-4 p-4 h-max gap-3">
        {!hasData ? (
          <p className="text-4xl font-extrabold text-center p-20">No data</p>
        ) : (
          wallets.map((x) => (
            <TabsTrigger
              key={x.id}
              value={x.id.toString()}
              className="flex-1 min-w-[280px] w-full rounded-xl h-72 bg-[#D9F3E2] group relative flex flex-col overflow-hidden shadow-empty transition-all hover:shadow-lg"
            >
              <FundWalletOverviewCard
                wallet={x}
                refreshWallets={refreshWallets}
              />
            </TabsTrigger>
          ))
        )}
      </TabsList>

      <ScrollBar orientation="horizontal" />
    </ScrollArea>
  );
};

export default FundWalletOverviewTabsList;
