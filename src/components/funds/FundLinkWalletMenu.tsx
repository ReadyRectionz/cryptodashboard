import { Dialog, DialogContent, DialogTrigger } from '@/components/ui/dialog';
import { CirclePlus } from 'lucide-react';
import FundLinkWalletForm from './FundLinkWalletForm';

interface FundLinkWalletMenuProps {
  fund_id: number;
  disabled: boolean;
  refreshWallets: () => void;
}

const FundLinkWalletMenu = ({
  fund_id,
  disabled,
  refreshWallets,
}: FundLinkWalletMenuProps) => {
  return (
    <Dialog>
      <DialogTrigger disabled={disabled}>
        <CirclePlus
          className="m-4 cursor-pointer"
          color={`${disabled ? '#d1d5db' : '#00B16A'}`}
        />
      </DialogTrigger>
      <DialogContent className="w-[350px] m-0 p-0">
        <FundLinkWalletForm fund_id={fund_id} refreshWallets={refreshWallets} />
      </DialogContent>
    </Dialog>
  );
};

export default FundLinkWalletMenu;
