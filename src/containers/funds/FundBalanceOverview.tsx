import FundChainTokenBalanceCard from '@/components/funds/FundChainTokenBalanceCard';
import FundBalanceHistoryCard from '@/components/funds/FundBalanceHistoryCard';
import { FundWithWalletExtended } from '@/actions/type';

interface FundBalanceOverviewProps {
  fund?: FundWithWalletExtended;
  showPlaceHolder: boolean;
}

const FundBalanceOverview = ({
  fund,
  showPlaceHolder,
}: FundBalanceOverviewProps) => {
  return (
    <div className="px-20 pt-10 pb-4">
      <p className="text-2xl font-bold py-5">Fund Balance Overview</p>
      <div className="flex flex-1 gap-8">
        <FundChainTokenBalanceCard
          fund={fund}
          showPlaceHolder={showPlaceHolder}
        />
        <FundBalanceHistoryCard fund={fund} showPlaceHolder={showPlaceHolder} />
      </div>
    </div>
  );
};

export default FundBalanceOverview;
