import FundProtocolTable from '@/components/funds/FundProtocolTable';
import { WalletDBExtended } from '@/lib/database/wallet/type';

interface FundProtocolOverviewProps {
  wallets: WalletDBExtended[];
  showPlaceHolder: boolean;
}

const FundProtocolOverview = ({
  wallets,
  showPlaceHolder,
}: FundProtocolOverviewProps) => {
  return (
    <div className="px-20 pt-10 pb-4">
      <p className="text-2xl font-bold py-5">Protocol Overview</p>
      <FundProtocolTable wallets={wallets} showPlaceHolder={showPlaceHolder} />
    </div>
  );
};

export default FundProtocolOverview;
