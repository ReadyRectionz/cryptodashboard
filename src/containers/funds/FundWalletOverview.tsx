import { Tabs } from '@/components/ui/tabs';

import { WalletDBExtended } from '@/lib/database/wallet/type';

import { Skeleton } from '@/components/ui/skeleton';
import FundLinkWalletMenu from '@/components/funds/FundLinkWalletMenu';
import FundWalletOverviewTabsList from '@/components/funds/FundWalletOverviewTabsList';
import FundWalletOverviewTabsContent from '@/components/funds/FundWalletOverviewTabsContent';

interface FundWalletTabsProps {
  wallets: WalletDBExtended[];
  refreshWallets: () => void;
}
const FundWalletOverviewTabs = ({
  wallets,
  refreshWallets,
}: FundWalletTabsProps) => {
  return (
    <Tabs>
      <FundWalletOverviewTabsList
        wallets={wallets}
        refreshWallets={refreshWallets}
      />
      <FundWalletOverviewTabsContent wallets={wallets} />
    </Tabs>
  );
};

interface FundWalletOverviewProps {
  fund_id: number;
  wallets: WalletDBExtended[];
  showPlaceHolder: boolean;
  refreshWallets: () => void;
}

const FundWalletOverview = ({
  fund_id,
  wallets,
  showPlaceHolder,
  refreshWallets,
}: FundWalletOverviewProps) => {
  return (
    <div className="px-20 pt-10 pb-4">
      <div className="flex">
        <p className="text-2xl font-bold py-5">Wallet Overview</p>

        <FundLinkWalletMenu
          fund_id={fund_id}
          disabled={showPlaceHolder}
          refreshWallets={refreshWallets}
        />
      </div>

      {showPlaceHolder ? (
        <Skeleton className="h-64" />
      ) : (
        <FundWalletOverviewTabs
          wallets={wallets}
          refreshWallets={refreshWallets}
        />
      )}
    </div>
  );
};

export default FundWalletOverview;
