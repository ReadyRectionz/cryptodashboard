import Image from 'next/image';
import LoginForm from '@/components/LoginForm';

const Login = () => {
  return (
    <div className="flex flex-col items-center p-20">
      <Image
        src="/assets/logo/levelstech.png"
        width={400}
        height={400}
        alt="Company logo"
      />
      <div className="bg-[#F2FBFF] mt-20 border-solid border-2 rounded-xl p-10">
        <LoginForm />
      </div>
    </div>
  );
};

export default Login;
