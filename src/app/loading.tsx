import Image from 'next/image';

interface LoadingProps {
  fillScreen?: boolean;
}

export default function Loading({ fillScreen = false }: LoadingProps) {
  return (
    <div
      className={`flex flex-col flex-grow ${fillScreen ? 'h-lvh' : 'h-full'}  w-full items-center justify-center bg-gray-50 bg-cover bg-center text-grey-500`}
    >
      <Image
        src="/assets/icons/spinner.svg"
        alt="spinner"
        width={100}
        height={100}
      />
      <p className="text-xl font-bold">Loading...</p>
    </div>
  );
}
