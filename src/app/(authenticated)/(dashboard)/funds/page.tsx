'use client';

import React, { useEffect, useState } from 'react';
import { fetchAllPoolsWithChainBalances } from '@/actions/fund';
import { FundWithWalletExtended } from '@/actions/type';
import Loading from '../../../loading';
import { Skeleton } from '@/components/ui/skeleton';

import dynamic from 'next/dynamic';

const LoadingFundOverViewCard = () => {
  return (
    <>
      <div className="absolute top-1/3">
        <Loading />
      </div>
      <Skeleton className="group flex min-h-[520px] w-full max-w-[600px] flex-col overflow-hidden rounded-xl bg-[#D9F3E2] transition-all shadow-lg md:min-h-[488px]" />
    </>
  );
};
const FundOverviewCard = dynamic(
  () => import('@/components/funds/FundOverviewCard'),
  {
    loading: () => <LoadingFundOverViewCard />,
  },
);

const PoolfolioOverview = () => {
  const [data, setData] = useState<FundWithWalletExtended[]>([]);
  const [error, setError] = useState<any>();
  const [loading, setLoading] = useState(true);

  const hasData = data.length > 0;
  const filler = [1, 2, 3, 4];
  useEffect(() => {
    setLoading(true);

    const fetchData = async () => {
      let { data, error } = await fetchAllPoolsWithChainBalances();
      if (error || !data) {
        setError(error);
        return;
      }
      setData(data);
    };

    fetchData().finally(() => {
      setLoading(false);
    });
  }, []);

  return (
    <div className="flex flex-wrap p-40 gap-10">
      {loading ? (
        <div className="flex flex-col items-center gap-5 w-full">
          <ul className="grid w-full grid-cols-2 gap-3 sm:grid-cols-1 lg:grid-cols-2 xl:gap-10">
            {filler.map((x) => (
              <li key={x} className="flex relative justify-center">
                <LoadingFundOverViewCard />
              </li>
            ))}
          </ul>
        </div>
      ) : !hasData ? (
        <div className="flex-center wrapper min-h-[200px] w-full flex-col gap-3 rounded-[14px] bg-grey-50 py-28 text-center">
          <h3 className="p-bold-20 md:h5-bold">No data</h3>
          <p className="p-regular-14">Please create funds in supabase</p>
        </div>
      ) : (
        <div className="flex flex-col items-center gap-5 w-full">
          <ul className="grid w-full grid-cols-2 gap-3 sm:grid-cols-1 lg:grid-cols-2 xl:gap-10">
            {data.map((x) => (
              <li key={x.uuid} className="flex relative justify-center">
                <FundOverviewCard fund={x} />
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default PoolfolioOverview;
