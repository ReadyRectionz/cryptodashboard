'use client';
import { fetchFundsWithWalletsBalancePKByUUID } from '@/actions/fund';
import { FundWithWalletExtended } from '@/actions/type';
import FundDetailHeader from '@/components/funds/FundDetailHeader';
import FundBalanceOverview from '@/containers/funds/FundBalanceOverview';
import FundProtocolOverview from '@/containers/funds/FundProtocolOverview';
import FundWalletOverview from '@/containers/funds/FundWalletOverview';
import { WalletDBExtended } from '@/lib/database/wallet/type';
import { useEffect, useState } from 'react';

interface PoolfolioProps {
  params: {
    id: string;
  };
}

const Poolfolio = ({ params: { id } }: PoolfolioProps) => {
  const [error, setError] = useState<any>('');
  const [loading, setLoading] = useState(true);
  const [fund, setFund] = useState<FundWithWalletExtended>();
  const [fundId, setFundId] = useState<number>(0);
  const [wallets, setWallets] = useState<WalletDBExtended[]>([]);
  const [totalBalance, setTotalBalance] = useState(0);
  const [changedToggle, setChangedToggle] = useState(false);

  const refreshWallets = () => {
    setChangedToggle(!changedToggle);
  };

  useEffect(() => {
    setLoading(true);

    const fetchFunds = async () => {
      let { data, error } = await fetchFundsWithWalletsBalancePKByUUID(id);
      if (error || !data) {
        setError(error);
        return;
      }
      setFund(data.fund);
      setFundId(data.fund.id);
      setWallets(data.wallet);
      setTotalBalance(
        data.wallet.reduce((acc, x) => (acc += x?.balance || 0), 0),
      );
    };

    fetchFunds().finally(() => {
      setLoading(false);
    });
  }, [id, changedToggle]);

  return (
    <>
      <FundDetailHeader
        fund={fund}
        totalBalance={totalBalance}
        showPlaceHolder={loading}
      />
      <FundBalanceOverview fund={fund} showPlaceHolder={loading} />
      <FundProtocolOverview wallets={wallets} showPlaceHolder={loading} />

      <FundWalletOverview
        fund_id={fundId}
        wallets={wallets}
        showPlaceHolder={loading}
        refreshWallets={refreshWallets}
      />
    </>
  );
};

export default Poolfolio;
