'use server';
import { PropsWithChildren, Suspense } from 'react';
import Sidebar from '@/components/Sidebar';
import Loading from '../../loading';
import { redirect } from 'next/navigation';
import supabaseServerSide from '@/utils/supabase/server';

export default async function AuthenticatedLayout(props: PropsWithChildren) {
  const { supabase } = await supabaseServerSide();
  if (!supabase) {
    redirect('/login');
  }
  const {
    data: { session },
  } = await supabase.auth.getSession();

  if (!session) {
    redirect('/login');
  }

  return (
    <div className="flex items-start justify-between">
      <Sidebar />
      <Suspense fallback={<Loading fillScreen={true} />}>
        <div className="w-full h-full">{props.children}</div>
      </Suspense>
    </div>
  );
}
