import { FundDB } from '@/lib/database/fund/type';
import { WalletDBExtended } from '@/lib/database/wallet/type';
import { Chain, Token } from '@/lib/debank/totalBalance/type';

export interface TotalChainBalance {
  total_usd_value: number;
  chain_object: { [id: string]: Chain };
}

export interface TotalTokenBalance {
  total_usd_value: number;
  token_object: { [id: string]: Token };
}

export interface WalletNetCurveOverview {
  id: number;
  name: string;
  pubkey?: string;
  entries: { timestamp: number; usd_value: number }[];
}

export interface WalletSnapshotOverview {
  id: number;
  name: string;
  pubkey?: string;
  entries: { time: string; value: number }[];
}

export interface FundWithWalletExtended extends FundDB {
  created_at: string;
  min_amount: number;
  source_of_funds_min_amount: number;
  total?: TotalChainBalance;
  total_token?: TotalTokenBalance;
  wallet: WalletDBExtended[];
}
