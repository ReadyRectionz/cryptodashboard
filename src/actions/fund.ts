'use server';

import {
  fetchFundIdWithWalletsByUUID,
  fetchFundsWithWalletsPubkey,
} from '@/lib/database/fund/actions';
import {
  decryptWalletPubkey,
  fetchWalletPubkey,
  getWalletSnapshots,
} from '@/lib/database/wallet/actions';
import { WalletDBExtended } from '@/lib/database/wallet/type';
import {
  fetchWalletTotalBalance,
  fetchWalletProtocols,
  fetchWalletChainBalances,
  fetchWalletsTokens,
  fetchNetCurve,
} from '@/lib/debank/totalBalance/actions';
import {
  FundWithWalletExtended,
  TotalChainBalance,
  TotalTokenBalance,
  WalletNetCurveOverview,
  WalletSnapshotOverview,
} from './type';

interface Response<T> {
  data: T | null;
  error: any;
}

const populateWalletTotalBalance = async (wallet: WalletDBExtended) => {
  wallet = await decryptWalletPubkey(wallet);

  if (!('balance' in wallet) && 'pubkey' in wallet && wallet.pubkey)
    wallet.balance = await fetchWalletTotalBalance(wallet.pubkey);

  return wallet;
};

export const noPopulateWalletTotalBalance = async (
  wallet: WalletDBExtended,
) => {
  if ('balance' in wallet) return wallet.balance;

  const pubkey =
    'pubkey' in wallet ? wallet.pubkey : await fetchWalletPubkey(wallet);

  return pubkey ? await fetchWalletTotalBalance(pubkey) : 0;
};

const populateFundTotal = async (fund: FundWithWalletExtended) => {
  const total: TotalChainBalance = { total_usd_value: 0, chain_object: {} };

  fund.wallet = await Promise.all(
    fund.wallet.map(async (x: WalletDBExtended) => {
      x = await decryptWalletPubkey(x);

      if (x.pubkey) {
        const totalBalance = await fetchWalletChainBalances(x.pubkey);

        total.total_usd_value += totalBalance.total_usd_value;

        totalBalance.chain_list
          .filter((x) => x.usd_value > 0)
          .forEach((x) => {
            if (total && x.id in total.chain_object) {
              total.chain_object[x.id].usd_value += x.usd_value;
            } else if (total) {
              total.chain_object[x.id] = x;
            }
          });
      }

      return x;
    }),
  );

  fund.total = total;

  return fund;
};

const populateFundToken = async (fund: FundWithWalletExtended) => {
  const total: TotalTokenBalance = { total_usd_value: 0, token_object: {} };

  fund.wallet = await Promise.all(
    fund.wallet.map(async (x: WalletDBExtended) => {
      x = await decryptWalletPubkey(x);

      if (x.pubkey) {
        const totalTokens = await fetchWalletsTokens(x.pubkey);

        totalTokens
          .filter((x) => x.is_verified)
          .forEach((x) => {
            total.total_usd_value += x.amount * x.price;

            if (total && x.id in total.token_object) {
              total.token_object[x.id].amount += x.amount;
              // total.token_object[x.id].raw_amount += x.raw_amosunt;
              // total.token_object[x.id].raw_amount_hex_str += x.raw_amount_hex_str;
            } else if (total) {
              total.token_object[x.id] = x;
            }
          });
      }

      return x;
    }),
  );

  fund.total_token = total;

  return fund;
};

const findUnkownPubkkeys = (wallets: WalletDBExtended[]) => {
  return wallets
    .filter((x) => !x.pubkey)
    .reduce((acc, x, i) => (i == 0 ? x.name : acc + ',' + x.name), '');
};

export const fetchFundsWithWalletsDecryptPKByUUID = async (uuid: string) => {
  const { data: fund, error } = await fetchFundIdWithWalletsByUUID(uuid);
  if (error || !fund) return { data: null, error };

  const ws = await Promise.all(
    fund[0].wallet.map(async (x) => await decryptWalletPubkey(x)),
  );

  return { data: { fund: fund[0], wallet: ws }, error: error };
};

export const fetchFundsWithWalletsBalancePKByUUID = async (uuid: string) => {
  const { data: fund, error } = await fetchFundIdWithWalletsByUUID(uuid);
  if (error || !fund) return { data: null, error };

  const ws = await Promise.all(
    fund[0].wallet.map(async (x) => await populateWalletTotalBalance(x)),
  );

  return { data: { fund: fund[0], wallet: ws }, error: error };
};

export const fetchAllPoolsWithChainBalances = async () => {
  const { data: fund, error } = await fetchFundsWithWalletsPubkey();
  if (error || !fund) return { data: null, error };

  const response = await Promise.all(
    fund.map(async (x: FundWithWalletExtended) => populateFundTotal(x)),
  );

  return { data: response, error: error };
};

export const fetchPoolChainBalances = async (fund: FundWithWalletExtended) => {
  const response = await populateFundTotal(fund);

  return { data: response.total, error: null };
};

export const fetchPoolTokenBalances = async (fund: FundWithWalletExtended) => {
  const response = await populateFundToken(fund);

  return { data: response.total_token, error: null };
};

export const fetchPoolProtocols = async (...wallets: WalletDBExtended[]) => {
  const unkownPubkeys = findUnkownPubkkeys([...wallets]);

  if (unkownPubkeys)
    return {
      data: null,
      error: `Cannot fetch protocols for wallets: ${unkownPubkeys}`,
    };

  const data = (
    await Promise.all(
      wallets.map(async (x) => {
        if (x.pubkey) return await fetchWalletProtocols(x.pubkey);
      }),
    )
  ).reduce((acc, x) => acc?.concat(x || []), []);

  return { data: data, error: null };
};

export const fetchPoolNetCurve = async (
  ...wallets: WalletDBExtended[]
): Promise<Response<WalletNetCurveOverview[]>> => {
  const unkownPubkeys = findUnkownPubkkeys([...wallets]);

  if (unkownPubkeys)
    return {
      data: null,
      error: `Cannot fetch net curve for wallets: ${unkownPubkeys}`,
    };

  const entries = await Promise.all(
    wallets.map(async (x) => {
      return {
        id: x.id,
        name: x.name,
        pubkey: x.pubkey,
        entries: await fetchNetCurve(x?.pubkey || ''),
      };
    }),
  );

  return {
    data: entries,
    error: null,
  };
};

export const fetchPoolSnapshots = async (
  ...wallets: WalletDBExtended[]
): Promise<Response<WalletSnapshotOverview[]>> => {
  const unkownPubkeys = findUnkownPubkkeys([...wallets]);

  if (unkownPubkeys)
    return {
      data: null,
      error: `Cannot fetch snapshot for wallets: ${unkownPubkeys}`,
    };

  let error = null;
  const entries = await Promise.all(
    wallets
      .filter((x) => x.active)
      .map(async (x) => {
        const { data, error: errSnap } = await getWalletSnapshots(x.id);

        if (errSnap || !data) {
          error = `Cannot fetch snapshot: ${errSnap}`;
        }
        return {
          id: x.id,
          name: x.name,
          pubkey: x.pubkey,
          entries: data || [],
        };
      }),
  );

  if (error) {
    return { data: null, error: error };
  }

  return {
    data: entries,
    error: null,
  };
};
